# import needed modules
import sqlite3
from classes.tag import Tag
from configQuotes import Config

# create a config object so we can access it's properties
config = Config()

# ---------------------------------------------

 
class Quote:

    # this is a contructor class
    # it holds properties of the object that can be 
    # referred to throughout via "self"
    def __init__(self):
        # put properties here

        # path is relative to root of app (app.py)
        # it comes from the config.py class in the root folder
        self.dbPath = config.dbName




    # this method gets all of the quotes
    def retrieveAllQuotes(self):

        with sqlite3.connect(self.dbPath) as con:

            # this makes the results come back as a dictionary
            con.row_factory = sqlite3.Row 
            # create cursor
            cur = con.cursor()
            # SQL statement with sort by descending order
            cur.execute("SELECT * FROM quotes ORDER BY timeSubmitted DESC")
            # get the results
            result = cur.fetchall()
            # close our connection
            cur.close()

            # if the result exists then return it to app.py else return False
            if result:
                return result
            else:
                return False






    # this method retrieves just the users quotes
    def retrieveUserQuotes(self,id_user):

        with sqlite3.connect(self.dbPath) as con:
 
            # this makes the results come back as a dictionary
            con.row_factory = sqlite3.Row 
            # create cursor
            cur = con.cursor()
            # SQL statement with sort by descending order
            cur.execute("SELECT * FROM quotes WHERE id_user = ? ORDER BY timeSubmitted DESC",[id_user])
            # get the results
            result = cur.fetchall()
            # close our connection
            cur.close()

            # if the result exists then return it to app.py else return False
            if result:
                return result
            else:
                return False
           



    def retrieveSingleQuote(self,quoteId,id_user):

        with sqlite3.connect(self.dbPath) as con:

            # this makes the results come back as a dictionary
            con.row_factory = sqlite3.Row 
            # create cursor
            cur = con.cursor()
            # SQL statement with sort by descending order
            cur.execute("SELECT * FROM quotes WHERE id = ? AND id_user = ?",[quoteId,id_user])
            # get the results
            quoteResult = cur.fetchone()

            if quoteResult:

                # go get all tags associated with the quote
                cur.execute("SELECT tags.tag AS tag FROM tags INNER JOIN quotes_tags ON quotes_tags.id_tags = tags.id WHERE quotes_tags.id_quotes = ?",[quoteId])
                tagsResult = cur.fetchall()
                # close our connection
                cur.close()
                con.commit()

                # print("tagsResult:",tagsResult)

                tagString = ""
                for tag in tagsResult:
                    tagString = tagString+", "+tag['tag']
                if tagString.startswith(", "):
                    tagString = tagString.lstrip(", ")

                # print("tagString:",tagString)

                returnDict = {"quote":quoteResult["quote"],"source":quoteResult["source"],"tagString":tagString}

                return returnDict
            else:
                return False





    # this method addQuote lets us insert a quote into the database
    def addQuote(self,quote,source,tagData,id_users):

        # this is a convenient way to open and close 
        # a database connection. Notice how we've used the self.dbPath
        # property of the user object
        with sqlite3.connect(self.dbPath) as con:

            # create cursor
            cur = con.cursor()
            # execute SQL. Notice how we've used ? as placeholders for the actual values. 
            # This is important to prevent SQL injection. The values we want to enter
            # into the database are given as a set in the exact order immediately after.
            result = cur.execute("INSERT INTO quotes (quote,source,id_user) VALUES (?,?,?)",(quote,source,id_users))
            # this commits the row into the quotes table
            con.commit()
            quoteId = cur.lastrowid
            cur.close()


            if result:

                tag = Tag()
                tagList = tag.addTags(tagData)

                if tagList:

                    success = tag.assignTags(quoteId,tagList)

                    if success:
                        return True
                    else:
                        return False

                else:
                    return False
            else:
                return False 

            # now we return True to app.py so that we can handle errors/success
            return True





    def deleteQuote(self,id_user,quoteId):

        with sqlite3.connect(self.dbPath) as con:

            cur = con.cursor()
            cur.execute("SELECT * FROM quotes WHERE id_user = ? AND id = ?",[id_user,quoteId])
            result = cur.fetchone()

            if result:
                cur.execute("DELETE FROM quotes WHERE id = ?",[quoteId])
                cur.close()
                return True
            else:
                cur.close()
                return False





    def editQuote(self,quoteId,quoteData,sourceData,tagData,id_user):

        with sqlite3.connect(self.dbPath) as con:

            cur = con.cursor()
            cur.execute("SELECT * FROM quotes WHERE id_user = ? AND id = ?",[id_user,quoteId])
            result = cur.fetchone()

            if result:
                cur.execute("UPDATE quotes SET quote=?, source=? WHERE id = ?",[quoteData,sourceData,quoteId])
                cur.close()
                con.commit()


                tags = Tag()
                tagList = tags.addTags(tagData)

                if tagList:
                    response = tags.assignTags(quoteId,tagList)

                    if response:
                        return True
                    else:
                        return response

                return True

            else:
                cur.close()
                return False
