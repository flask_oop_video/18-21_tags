# import flash application
from flask import Flask, render_template, request, redirect, url_for, flash, session, logging
# import our config file config.py
from configQuotes import Config
# import functools
from functools import wraps 
# import our classes in the classes folder
from classes.user import User
from classes.quote import Quote
from classes.tag import Tag

import classes.forms

# ---------------------------------------------
 
# create an object with our Config class properties
config = Config()

# instantiate flask
app = Flask(__name__)

# set the secret key for encryption
app.secret_key = config.secret_key

# ---------------------------------------------

# checks if user is logged in
def is_loggged_in(f):
    @wraps(f)
    # We use *args and **kwargs because we don't know how many
    # parameters (arguements) will be passed to each function
    # that we're testing.
    def wrap(*args,**kwargs):
        # if the user is logged in then it's business as usual
        if 'logged_in' in session:
            return f(*args,**kwargs)
        else:
            # if the user isn't logged in then flash them a message
            # and boot them out to the login page.
            flash("Unauthorised access. Please log in.","danger")
            return redirect(url_for('login'))
    return wrap

# ---------------------------------------------

# this route defines the behavior at the root endpoint of our site
@app.route("/")
def home():

    quote = Quote()
    allQuotes = quote.retrieveAllQuotes()

    # this reaches into our template folder and renders the home.html page
    return render_template("home.html",allQuotes = allQuotes)

# ---------------------------------------------


# this route is for the 'about' endpoint
@app.route("/about")
def about():
    # this reaches into our template folder and renders the about.html page
    return render_template("about.html")

# ---------------------------------------------


@app.route("/user")
# this will only be available to logged in users
@is_loggged_in
def user():

    quote = Quote()
    userQuotes = quote.retrieveUserQuotes(session['user_id'])

    if userQuotes:
        return render_template("user.html", name=session['username'],userQuotes = userQuotes)
    else:
        flash("Something went wrong. No quotes exist for user: "+session['username'],"danger")
        return redirect(url_for("home"))
    
 

# ---------------------------------------------

@app.route("/quote", methods=["GET", "POST"])
# this will only be available to logged in users
@is_loggged_in
def quote():

    form = classes.forms.QuoteForm(request.form)

    tags = Tag()
    allTags = tags.retrieveAllTags()


    # because this form submits to itself we will see if 
    # anything has been submitted and treat it differently
    # this only works if all fields have validated correctly
    if request.method == "POST" and form.validate():

        # grab user inputs from form 
        quoteIn = form.quote.data
        source = form.source.data
        tagData = form.tags.data

        # create an instance of quote object from classes/quote 
        quote = Quote()

        # call the addQuote method in our object, passing it the quote details
        success = quote.addQuote(quoteIn,source,tagData,session['user_id'])

        # if success:   is the same thing as   if success == True
        if success:
            flash("You've entered a quote into the collection.","success")
            return redirect(url_for("quote"))
        else:
            # if insert returns False then notify and let them try again
            flash("There was a problem adding your quote.","danger")

    # this is the default behavior for this page (shows the quote form)
    return render_template("quote.html", form = form, allTags = allTags)
 


# ---------------------------------------------


@app.route("/deleteQuote/<string:quoteId>")
def quoteDelete(quoteId):

    quote = Quote()
    success = quote.deleteQuote(session['user_id'],quoteId)

    if success:
        flash("Your quote was successfully deleted from the collection.","success")
    else:
        flash("Something went wrong. Either that quote doesn't exist or you don't have authorisation to delete it.","danger")
    
    return redirect(url_for("user"))



# ---------------------------------------------


@app.route("/editQuote/<string:quoteId>", methods=["GET", "POST"])
def quoteEdit(quoteId):

    form = classes.forms.QuoteForm(request.form)
    quote = Quote()

    if request.method == "POST" and form.validate():

        # send new version to database

        quoteData = form.quote.data 
        sourceData = form.source.data
        tagData = form.tags.data

        success = quote.editQuote(quoteId,quoteData,sourceData,tagData,session['user_id'])

        if success:
            flash("Your quotation has been updated.","success")
        else:
            flash("There was a problem updating your quote.","danger")

    
    else:

        quoteDetails = quote.retrieveSingleQuote(quoteId,session['user_id'])

        if quoteDetails:

            tags = Tag()
            allTags = tags.retrieveAllTags()

            form.quote.data = quoteDetails['quote']
            form.source.data = quoteDetails['source']
            form.tags.data = quoteDetails['tagString']

            return render_template("quote.html", form=form, allTags=allTags)
        else:
            flash("There was a problem retrieving details for this quote.","danger")

 
    
    return redirect(url_for("user"))


# ---------------------------------------------


# this handles the registration endpoint
@app.route("/register", methods=["GET", "POST"])
def register():

    # this creates an object that contains the RegisterForm fields
    # and validation rules
    form = classes.forms.RegisterForm(request.form)

    # because this form submits to itself we will see if 
    # anything has been submitted and treat it differently
    # this only works if all fields have validated correctly
    if request.method == "POST" and form.validate():

        # grab user inputs from form 
        username = form.username.data
        email = form.email.data
        password = form.password.data

        # create a user object so that we can access the user methods
        # this comes from classes/user.py
        user = User()

        # here we call the insertUser method on the user class
        # it will return a boolean of either True or False
        success = user.insertUser(username,password,email)

        # if success:   is the same thing as   if success == True
        if success:
            # if user successfully registers we let them know and send them to login
            flash("You have now registered.","success")
            return redirect(url_for("login"))
        else:
            # if insertUser returns False then notify and let them try again
            flash("This user already exists. Try again.","danger")
            return redirect(url_for("register"))

    # this is what happens when somone just visits the register
    # enpoint for the first time (before they submit something)
    return render_template("register.html", form=form)


# ---------------------------------------------


# this route handles the login endpoint
@app.route("/login", methods=["GET", "POST"])
def login():
    # this creates an object that contains the 
    # LoginForm fields and validation rules
    form = classes.forms.LoginForm(request.form)

    # because this form submits to itself we will see if 
    # anything has been submitted and treat it differently
    # this only works if all fields have validated correctly
    if request.method == "POST" and form.validate():

        # grab user inputs from form 
        username = form.username.data
        passwordAttempt = form.password.data

        # create a user object so that we can access the 
        # user methods this comes from classes/user.py
        user = User()

        # here we call the authenticateUser method on the user class
        # it will return a boolean of False or the user's ID
        auth = user.authenticateUser(username, passwordAttempt)

        # if authentication was successful
        if auth:
            # create a user session. session comes from flask
            session['logged_in'] = True
            session['username'] = username
            session['user_id'] = auth
            print(session['user_id'])
            # notify user and send them to their dashboard
            flash("You are now logged in, "+username+".","success")
            return redirect(url_for("user", name=username))
        
        else:
            # if authentication failed 
            error = "Incorrect username or password."
            return render_template("login.html",form=form,error=error)

    # this is what happens when somone just visits the login
    # enpoint for the first time (before they submit something)
    return render_template("login.html",form=form)

# ---------------------------------------------



# this is the logout endpoint
@app.route("/logout")
# this will only be available to logged in users
@is_loggged_in
def logout():

    # this gets rid of the current session (logs user out)
    session.clear()

    # notify and send them to the login endpoint
    flash("You are now logged out.","success")
    return redirect(url_for("login"))

# ---------------------------------------------

# endpoint for custom 404 error page
@app.errorhandler(404)
def page_not_found(e):
    # log the error message to the terminal
    app.logger.info(e)
    # send the user to our custom error page
    return render_template("errors/404.html")

# ---------------------------------------------

# if this is the actual file being directly executed
if __name__ == "__main__":

    # finally, this runs our app
    # to get rid of debug mode just set it to False or remove it
    app.run(debug=True)